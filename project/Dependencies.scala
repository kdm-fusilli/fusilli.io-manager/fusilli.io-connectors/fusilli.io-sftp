import sbt._

// format: OFF
object Version {

  val Log4j                   = "1.7.30"
  val Akka                    = "2.6.9"
  val AkkaStreamAlpakkaFtp    = "2.0.2"
  val AkkaStreamAlpakkaCSV    = "2.0.1"
  val KafkaClients            = "2.7.0"
  val OrgJson                 = "20210307"
  val JSch                    = "0.1.55"
  val TypesafePlay            = "2.9.2"
  val AkkaStreamKafka         = "2.0.4"
  val AkkaCluster             = "2.6.13"
}

object Library {
  // External Libraries
  val Slf4j          = "org.slf4j" % "slf4j-simple"    % Version.Log4j
//  val Log4jOverSlf4j          = "org.slf4j"              % "log4j-over-slf4j"         % Version.Log4j
  val AkkaStream              = "com.typesafe.akka"  %% "akka-stream"               % Version.Akka
  val AkkaTyped             =   "com.typesafe.akka" %% "akka-actor-typed" % Version.Akka
  val AkkaStreamAlpakkaFtp    = "com.lightbend.akka" %% "akka-stream-alpakka-ftp"    % Version.AkkaStreamAlpakkaFtp
  val AkkaStreamAlpakkaCSV    = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "2.0.1"
  val KafkaClient             = "org.apache.kafka"      %% "kafka"                    % Version.KafkaClients
  val OrgJson                 = "org.json" % "json" % Version.OrgJson
  val JSch                    = "com.jcraft" % "jsch" % Version.JSch
  val TypesafePlay            = "com.typesafe.play" %% "play-json" % Version.TypesafePlay
  val AkkaStreamKafka         = "com.typesafe.akka" %% "akka-stream-kafka" % Version.AkkaStreamKafka
  val AkkaCluster             = "com.typesafe.akka" %% "akka-cluster" % Version.AkkaCluster


}

// format: ON
