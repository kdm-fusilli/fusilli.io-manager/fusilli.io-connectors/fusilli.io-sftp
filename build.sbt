import Library._

organization in ThisBuild := "io.fusilli"
version in ThisBuild := "0.1.0"
name := "fusilli.io-sftp"

scalaVersion in ThisBuild := "2.13.5"

def dockerSettings = Seq(
  dockerBaseImage := "adoptopenjdk/openjdk8",
  dockerRepository := sys.props.get("docker.registry")
)


libraryDependencies ++= Vector(
  Slf4j,
  AkkaStream,
  AkkaTyped,
  AkkaStreamAlpakkaFtp,
  AkkaStreamAlpakkaCSV,
  KafkaClient,
  OrgJson,
  JSch,
  TypesafePlay

)
javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
