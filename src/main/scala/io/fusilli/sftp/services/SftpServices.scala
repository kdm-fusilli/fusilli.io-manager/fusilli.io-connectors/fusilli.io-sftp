package io.fusilli.sftp.services

import com.jcraft.jsch.{ChannelSftp, JSch, JSchException}
import io.fusilli.sftp.config.ConfigApp.{getInputConfigJson, getOutputConfigJson}
import io.fusilli.sftp.models.Models
import io.fusilli.sftp.services.KafkaServices.{getKafkaConsumer, getKafkaLogProducer, getKafkaProducer}
import io.fusilli.sftp.services.LoggingServices.sendLog
import kafka.common.KafkaException
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.config.ConfigException
import org.json.JSONObject
import org.slf4j.{Logger, LoggerFactory}

import java.io.{FileNotFoundException, InputStream}
import java.time.Duration
import java.util
import java.util.Properties
import scala.io.Source.fromFile
import scala.jdk.CollectionConverters.IterableHasAsScala

object SftpServices {

  private final val log: Logger = LoggerFactory.getLogger("fusilli.io-sftp")

  def getSource(cfg: String): Unit = {
    try {
      val outputQueue = getOutputConfigJson()
      val kafkaLogProducer = getKafkaLogProducer(outputQueue.host, outputQueue.port)

      sendLog(cfg, "START", kafkaLogProducer, "0")
      log.info("Retrieving source...")

      val jsonCfg: JSONObject = new JSONObject(cfg)

      val host: String = jsonCfg.get("host").toString
      val port: String = jsonCfg.get("port").toString
      val path: String = jsonCfg.get("schema_path").toString
      val user: String = jsonCfg.get("repo_user").toString
      val password: String = jsonCfg.get("repo_password").toString

      val fileDir = "data/input.csv"

      log.info("Connecting to SFTP...")
      createConnection(host, port, path, user, password)
      log.info("Connection with SFTP established.")

      log.info("Creating Kafka Producer...")
      val producer = getKafkaProducer(outputQueue.host, outputQueue.port)
      var data: JSONObject = new JSONObject()
      var header: JSONObject = new JSONObject()
      var payload: JSONObject = new JSONObject()
      val bufferedSource = fromFile(fileDir)

      for (line <- bufferedSource.getLines) {
        val cols = line.split(";").map(_.trim) // TODO get separator from cfg
        data = new JSONObject()
        payload = new JSONObject()
        header = new JSONObject()
        data.put("PROVINCIA", cols(0))
        data.put("COMUNE", cols(1))
        data.put("CODICE_INTERVENTO", cols(2))
        data.put("DESCRIZIONE_INTERVENTO", cols(3))
        data.put("LUNGHEZZA_TRATTA_FO", cols(4))
        data.put("PROPRIETARIO_FO", cols(5))
        header.put("status", "running")
        payload.put("header", header)
        payload.put("data", data)
        log.info("Sending row to Kafka...")
        producer.send(new ProducerRecord[String, String](outputQueue.topic, "keyName", payload.toString()))
      }
      bufferedSource.close()

      data = new JSONObject()
      payload = new JSONObject()
      header = new JSONObject()
      data.put("test", "test")
      header.put("status", "end")
      payload.put("header", header)
      payload.put("data", data)

      producer.send(new ProducerRecord[String, String](outputQueue.topic, "keyName", payload.toString()))

      sendLog(cfg, "END", kafkaLogProducer, "100")
      log.info("All rows sent to Kafka.")

    } catch {
      case f: FileNotFoundException => log.error("Unable to find file.")
      case k: KafkaException => log.error("Unable to connect to Kafka.")
      case c: ConfigException => log.error("Unable to retrieve config for Kafka")
      case u: JSchException => log.error("Unable to connect to SFTP. Wrong credentials.")
    }
  }

  def getSink(cfg: String): Unit = {
    try {
      log.info("Retrieving sink...")
      val inputConfig = getInputConfigJson()
      val jsonCfg: JSONObject = new JSONObject(cfg)

      val host: String = jsonCfg.get("host").toString
      val port: String = jsonCfg.get("port").toString
      val schema: String = jsonCfg.get("schema_path").toString
      val user: String = jsonCfg.get("repo_user").toString
      val password: String = jsonCfg.get("repo_password").toString

      log.info("Creating Kafka Consumer...")
      val consumer = getKafkaConsumer(inputConfig.host, inputConfig.port)
      consumer.subscribe(util.Arrays.asList(inputConfig.topic))
      var file: InputStream = null
      while (true) {
        val record = consumer.poll(Duration.ofMillis(1000)).asScala
        // TODO put each line in the file
      }

      log.info("Connecting to SFTP...")
      createConnection(host, port, schema, user, password, file) // TODO put the created file in this method
      log.info("Connection with SFTP established.")

    }
    catch {
      case f: FileNotFoundException => log.error("Unable to find file.")
      case k: KafkaException => log.error("Unable to connect to Kafka.")
      case c: ConfigException => log.error("Unable to retrieve config for Kafka")
    }
  }

  def createConnection(host: String, port: String, path: String, user: String, password: String): Unit = {
    val config: Properties = new Properties();
    config.put("StrictHostKeyChecking", "no"); // TODO secure access. this is a workaround
    val jsch: JSch = new JSch()
    val session = jsch.getSession(user, host, port.toInt)
    session.setPassword(password)
    session.setConfig(config) // TODO secure access. this is a workaround
    session.connect()
    val sftpChannel: ChannelSftp = session.openChannel("sftp").asInstanceOf[ChannelSftp]
    sftpChannel.connect()
    sftpChannel.get(path + "collegamentiinfibraottica2017.csv", "data/input.csv")
    session.disconnect()
  }

  def createConnection(host: String, port: String, path: String, user: String, password: String, file: InputStream): Unit = {
    val config: Properties = new Properties();
    config.put("StrictHostKeyChecking", "no"); // TODO secure access. this is a workaround
    val jsch: JSch = new JSch()
    val session = jsch.getSession(user, host, port.toInt)
    session.setPassword(password)
    session.setConfig(config) // TODO secure access. this is a workaround
    session.connect()
    val sftpChannel: ChannelSftp = session.openChannel("sftp").asInstanceOf[ChannelSftp]
    sftpChannel.connect()
    sftpChannel.put(file, "sftpuser/output-test/" + "collegamentiinfibraottica.csv")
    session.disconnect()
  }

  def getEntity(): Models.Entity = {
    val attributes: Seq[Models.Attribute] = Seq(
      new Models.Attribute("PROVINCIA", "varchar(250)", "", ""),
      new Models.Attribute("COMUNE", "varchar(250)", "", ""),
      new Models.Attribute("CODICE_INTERVENTO", "varchar(50)", "", ""),
      new Models.Attribute("DESCRIZIONE_INTERVENTO", "varchar(250)", "", ""),
      new Models.Attribute("LUNGHEZZA_TRATTA_FO", "varchar(50)", "", ""),
      new Models.Attribute("PROPRIETARIO_FO", "varchar(50)", "", ""),
    )
    Models.Entity(1, "collegamentiinfibraottica.csv", "csv", ";", attributes, 1234, 1)
  }
}