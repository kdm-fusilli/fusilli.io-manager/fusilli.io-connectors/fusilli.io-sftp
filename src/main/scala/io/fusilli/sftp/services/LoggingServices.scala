package io.fusilli.sftp.services

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.json.JSONObject

object LoggingServices {
  def sendLog(cfg: String, status: String, logProducer: KafkaProducer[String, String], percentage: String): Unit = {
    val jsonCfg: JSONObject = new JSONObject(cfg)
    val logQueue = "fusilli_pipelines_logger"

    val instanceId: String = System.getenv("""instance_id""")
    val pipelineId: String = System.getenv("""pipeline_id""")
    val policiesId: String = System.getenv("""policies_id""")
    val version: String = System.getenv("""version""")
    val jobConfigurationId: String = jsonCfg.get("job_configuration_id").toString

    val jsonLogger: JSONObject = new JSONObject()
    jsonLogger.put("instance_id", instanceId)
    jsonLogger.put("pipeline_id", pipelineId)
    jsonLogger.put("policies_id", policiesId)
    jsonLogger.put("version", version)
    jsonLogger.put("job_configuration_id", jobConfigurationId)
    jsonLogger.put("status", status)
    jsonLogger.put("message", "")
    jsonLogger.put("timestamp", System.currentTimeMillis().toString)
    jsonLogger.put("percentage", percentage)

    logProducer.send(new ProducerRecord[String, String](logQueue, "keyName", jsonLogger.toString))
  }
}
