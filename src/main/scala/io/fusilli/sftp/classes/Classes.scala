package io.fusilli.sftp.classes

import play.api.libs.json.{Format, JsValue, Json}


object Classes {

  case class FusilliQueue(
                           host: String,
                           port: Int,
                           group_id: String,
                           topic: String,
                           user: String,
                           password: String
                         )

  object FusilliQueue {
    implicit val format: Format[FusilliQueue] = Json.format[FusilliQueue]
  }

  case class Attribute(
                        attr_name:String,
                        attr_type: String,
                        format: String,
                        length: String
                      )
  object Attribute{
    implicit val format: Format[Attribute] = Json.format[Attribute]
  }

  case class Entity(
                     entity_id: Int,
                     entity_name: String,
                     entity_type: String,
                     separator: String,
                     attributes: Seq[Attribute],
                     connection_edge: Int,
                     entity_instances: Int
                   )
  object Entity{
    implicit val format: Format[Entity] = Json.format[Entity]
  }

  case class Repo(
                   repo_name:String,
                   repo_user:String,
                   repo_password: String,
                   repo_type: String,
                   job_configuration_id: Int,
                   host: String,
                   port: Int,
                   schema_path: String,
                   purpose: String,
                   entities: Seq[Entity]
                 )
  object Repo{
    implicit val format: Format[Repo] = Json.format[Repo]
  }

  case class JsonMessage(
                          header: Header,
                          data: JsValue,
                        )

  object JsonMessage {
    implicit val format: Format[JsonMessage] = Json.format[JsonMessage]
  }

  case class Header(
                     status: String
                   )

  object Header {
    implicit val format: Format[Header] = Json.format[Header]
  }


}
