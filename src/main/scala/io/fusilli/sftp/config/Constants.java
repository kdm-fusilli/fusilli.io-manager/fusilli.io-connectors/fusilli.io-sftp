package io.fusilli.sftp.config;

public enum Constants {

    input_edges,
    output_edges,
    connector_config,
    log_queue
}
