package io.fusilli.sftp.config
import io.fusilli.sftp.classes.Classes.{FusilliQueue, Repo}
import io.fusilli.sftp.config.Constants._
import play.api.libs.json.{JsArray, JsValue, Json}

object ConfigApp {


  def getKafkaLogHost(): JsValue = {
    val input_edges_str = System.getenv(log_queue.toString)
    val json: JsValue = Json.parse(input_edges_str)
    //TODO: return host:port for log connection
    json
  }

  def getInputConfigJson(): FusilliQueue = {
    val input_edges_str = System.getenv(input_edges.toString)
    val json: FusilliQueue = (Json.parse(input_edges_str) \ "input_edges").as[JsArray].apply(0).as[FusilliQueue]
    //println("\n\n\n" + json)
    json
  }


  def getOutputConfigJson(): FusilliQueue = {
    val output_edges_str = System.getenv(output_edges.toString)
    val json: FusilliQueue = (Json.parse(output_edges_str) \ "output_edges").as[JsArray].apply(0).as[FusilliQueue]
    //println("\n\n\n" + json)
    json
  }


  def getConnectorConfigJson(): Repo = {
    val connector_config_str = System.getenv(connector_config.toString)
    val json: Repo = Json.parse(connector_config_str).as[Repo]
    //println("\n\n\n" + json)
    json
  }

}
