package io.fusilli.sftp

import akka.actor.{Actor, ActorSystem, Props}
import io.fusilli.sftp.services.SftpServices
import org.json.JSONObject

object Main extends App {

  class SftpActor(cfg: String) extends Actor {
    override def receive: Receive = {
      case "input" => SftpServices.getSource(cfg)
      case "output" => SftpServices.getSink(cfg)
    }
  }

  val configString = System.getenv("""connector_config""")

  val system = ActorSystem("SftpSystem")
  val actor = system.actorOf(Props(new SftpActor(configString)), "SftpActor")

  val jsonCfg: JSONObject = new JSONObject(configString)
  val purpose: String = jsonCfg.get("purpose").toString

  actor ! purpose

}
