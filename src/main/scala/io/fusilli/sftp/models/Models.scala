package io.fusilli.sftp.models

object Models {
  case class Policy(
                     entity_id: Int,
                     attrs: Seq[PolicyAttribute]
                   )


  case class Attribute(
                        attr_name: String,
                        attr_type: String,
                        format: String,
                        length: String
                      )


  case class PolicyAttribute(
                              attr_name: String,
                              policy_behaviour: String,
                              parameter: String
                            )


  case class Entity(
                     entity_id: Int,
                     entity_name: String,
                     entity_type: String,
                     separator: String,
                     attributes: Seq[Attribute],
                     connection_edge: Int,
                     entity_instances: Int
                   )


  case class Repo(
                   repo_name: String,
                   repo_user: String,
                   repo_password: String,
                   repo_type: String,
                   job_configuration_id: Int,
                   host: String,
                   port: Int,
                   schema_path: String,
                   delay: Int,
                   entities: Seq[Entity]
                 )

  case class ConfigValue(
                          input_attrs: Seq[String],
                          output_attrs: Seq[String],
                        )

  case class Job(
                  job_configuration_id: Int,
                  job_name: String,
                  input_edges: Seq[Int],
                  output_edges: Seq[Int],
                  job_instances: Int,
                  job_configs: Seq[ConfigValue]
                )


}
